class Pokemon:
    def __init__(self, name, primary_type, max_hp):
        self.name         = name
        self.primary_type = primary_type
        self.max_hp       = max_hp
        self.hp           = max_hp

    def __str__(self):
        return f"{self.name} ({self.primary_type} : {self.hp}/{self.max_hp})"

    def feed(self):
        if self.hp < self.max_hp:
            self.hp += 10
            print(f"{self.name} has now {self.hp}")
        else:
            print(f"{self.name} is full.")

    def battle(self, other):
        #print(f"Battle: {self.name} , {other.name}") 
        result = self.typewheel(self.primary_type, other.primary_type)
        print(f"{self.name} fought {other.name} and {result}")
        if result == "lost":
            self.hp -= 10
        if result == "won":
            other.hp -= 10
        print(f"{self.name} HP is now {self.hp}")
        print(f"{other.name} HP is now {other.hp}")
        print(f"\n\n")
        if self.hp <= 0 and other.hp <= 0:
            print(f"Both pokemons fainted. Battle ended up tied.")
            return 0
        elif self.hp <= 0:
            print(f"{self.name} fainted. Battle finished.")
            return 0
        elif other.hp <= 0:
            print(f"{other.name} fainted. Battle finished.")
            return 0

    @staticmethod
    def typewheel(first_type, second_type):
        result = {0 : "lost", 1 : "won", -1 : "tied"}
        # map types and results
        game_map = {"water" : 0, "fire" : 1, "grass" : 2}

        # implement win-lose matrix
        wl_matrix = [
                [-1,1,0], # water
                [0,-1,1], # fire
                [1,0,-1]  # grass
                ]
        # declare winner
        wl_result = wl_matrix[game_map[first_type]][game_map[second_type]]
        return result[wl_result]

if __name__ == "__main__":
    b = Pokemon(name="Bulbassaur", primary_type="grass", max_hp=20)
    c = Pokemon(name="Charmander", primary_type="fire", max_hp=30)
    b.battle(c)
    c.battle(b)
    b.battle(b)

